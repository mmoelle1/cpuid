#include <stdint.h>
#include <cstddef>
#include <exception>
#include <string>
#include <vector>
#include <iostream>

#if defined(__APPLE__)
  #if !defined(__has_include)
    #define APPLE_SYSCTL
  #elif __has_include(<sys/sysctl.h>)
    #define APPLE_SYSCTL
  #endif
#endif

#if defined(_WIN32)

#include <windows.h>
#include <iterator>
#include <map>

#endif

#if defined(__i386__) || \
    defined(_M_IX86) || \
    defined(__x86_64__) || \
    defined(_M_X64) || \
    defined(_M_AMD64)
  #define IS_X86
#endif

#if defined(IS_X86)

#include <algorithm>

// Check if compiler supports <intrin.h>
#if defined(__has_include)
  #if __has_include(<intrin.h>)
    #define HAS_INTRIN_H
  #endif
#elif defined(_MSC_VER)
  #define HAS_INTRIN_H
#endif

// Check if compiler supports CPUID
#if defined(HAS_INTRIN_H)
  #include <intrin.h>
  #define MSVC_CPUID
#elif defined(__GNUC__) || \
      defined(__clang__)
  #define GNUC_CPUID
#endif

using namespace std;

namespace {

/// CPUID is not portable across all x86 CPU vendors and there
/// are many pitfalls. For this reason we prefer to get CPU
/// information from the operating system instead of CPUID.
/// We only use CPUID for getting the CPU name on Windows x86
/// because there is no other way to get that information.
///
void cpuId(int cpuInfo[4], int eax)
{
#if defined(MSVC_CPUID)
  __cpuid(cpuInfo, eax);
#elif defined(GNUC_CPUID)
  int ebx = 0;
  int ecx = 0;
  int edx = 0;

  #if defined(__i386__) && \
      defined(__PIC__)
    // in case of PIC under 32-bit EBX cannot be clobbered
    __asm__ ("movl %%ebx, %%edi;"
             "cpuid;"
             "xchgl %%ebx, %%edi;"
             : "+a" (eax),
               "=D" (ebx),
               "=c" (ecx),
               "=d" (edx));
  #else
    __asm__ ("cpuid;"
             : "+a" (eax),
               "=b" (ebx),
               "=c" (ecx),
               "=d" (edx));
  #endif

  cpuInfo[0] = eax;
  cpuInfo[1] = ebx;
  cpuInfo[2] = ecx;
  cpuInfo[3] = edx;
#else
  // CPUID is not supported
  eax = 0;

  cpuInfo[0] = eax;
  cpuInfo[1] = 0;
  cpuInfo[2] = 0;
  cpuInfo[3] = 0;
#endif
}

/// Remove all leading and trailing
/// space characters.
///
void trimString(string& str)
{
  string spaceChars = " \f\n\r\t\v";
  size_t pos = str.find_first_not_of(spaceChars);
  str.erase(0, pos);

  pos = str.find_last_not_of(spaceChars);
  if (pos != string::npos)
    str.erase(pos + 1);
}

} // namespace

#endif

using namespace std;

namespace {

string getCpuName()
{
  string cpuName;

#if defined(IS_X86)
  // Get the CPU name using CPUID.
  // Example: Intel(R) Core(TM) i7-6700 CPU @ 3.40GHz
  // https://en.wikipedia.org/wiki/CPUID

  int cpuInfo[4] = { 0, 0, 0, 0 };
  cpuId(cpuInfo, 0x80000000);
  vector<int> vect;

  // check if CPU name is supported
  if ((unsigned) cpuInfo[0] >= 0x80000004u)
  {
    cpuId(cpuInfo, 0x80000002);
    copy_n(cpuInfo, 4, back_inserter(vect));

    cpuId(cpuInfo, 0x80000003);
    copy_n(cpuInfo, 4, back_inserter(vect));

    cpuId(cpuInfo, 0x80000004);
    copy_n(cpuInfo, 4, back_inserter(vect));

    vect.push_back(0);
    cpuName = (char*) vect.data();
    trimString(cpuName);
  }
#endif

  return cpuName;
}

} // namespace

int main()
{
  std::cout << getCpuName() << std::endl;
  return 0;
}
