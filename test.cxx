#include <cstring>

#if defined(_WIN32) || defined(_WIN64)
#   include <windows.h>
#elif __APPLE__
#   include <sys/utsname.h>
#   include <sys/sysctl.h>
#elif __linux__
#   if defined(__x86_64__) && ( defined(__GNUC__) || defined(__clang__) || defined(__INTEL_COMPILER) || defined(__SUNPRO_CC))
#      include <cpuid.h>
#      include <unistd.h>
#elif defined(__aarch64__)
#      include <unistd.h>
#   endif
#elif __unix__
#endif

#include <iostream>

int main()
{
  
#if __linux__
#   if defined(__x86_64__) && ( defined(__GNUC__) || defined(__clang__) || defined(__INTEL_COMPILER) || defined(__SUNPRO_CC))
  
  char CPUBrandString[0x40];
  unsigned int CPUInfo[4] = {0,0,0,0};
  
  __cpuid(0x80000000, CPUInfo[0], CPUInfo[1], CPUInfo[2], CPUInfo[3]);
  unsigned int nExIds = CPUInfo[0];
  
  memset(CPUBrandString, 0, sizeof(CPUBrandString));
  
  for (unsigned int i = 0x80000000; i <= nExIds; ++i)
    {
      __cpuid(i, CPUInfo[0], CPUInfo[1], CPUInfo[2], CPUInfo[3]);
      
      if (i == 0x80000002)
        memcpy(CPUBrandString, CPUInfo, sizeof(CPUInfo));
      else if (i == 0x80000003)
        memcpy(CPUBrandString + 16, CPUInfo, sizeof(CPUInfo));
      else if (i == 0x80000004)
        memcpy(CPUBrandString + 32, CPUInfo, sizeof(CPUInfo));
    }

  std::cout << CPUBrandString << std::endl;
  
  long pages = sysconf(_SC_PHYS_PAGES);
  long page_size = sysconf(_SC_PAGE_SIZE);
  
  std::cout << std::to_string(pages * page_size / 1024 / 1024)+" MB\n";

#elif defined(__aarch64__)

  std::cout << "AARCH64\n";

  long pages = sysconf(_SC_PHYS_PAGES);
  long page_size = sysconf(_SC_PAGE_SIZE);
  
  std::cout << std::to_string(pages * page_size / 1024 / 1024)+" MB\n";
  
#elif defined (__arm__)

  std::cout << "ARM 32bit\n";
  
#endif
  
#endif
  
  return 0;
}
